# VizBlocks Server

Set up a remote VizBlocks server using Docker.

## Development

### Docker set up
1. [Install Docker and Docker Compose](https://docs.docker.com/compose/install/).
2. [Install Mosquitto](https://mosquitto.org/download/).
3. Duplicate the `dev_config_example` folder and rename it to `dev_config`.
4. On lines 126 and 127 in [dev_config/node-red-data/settings.js](dev_config/node-red-data/settings.js), replace `<NODE_RED_DEV_USER>` and `<NODE_RED_DEV_PASSWORD>`  with the username and password that you'd like to use. *Note: passwords should be encrypted using bcrypt, see [here](https://nodered.org/docs/user-guide/runtime/securing-node-red)*.
5. In [dev_config/mosquitto/credentials.txt](dev_config/mosquitto/credentials.txt), replace `<MQTT_DEV_USER>` and `<MQTT_DEV_PASSWORD>`  with the plain text username and password that you want to use for MQTT authentication (e.g. `user:password`).
6. In a terminal navigate to the project root and run the following command to encrypt your MQTT password file:
```bash
mosquitto_passwd -U dev_config/mosquitto/credentials.txt
```
7. In a terminal, navigate to the project root and use docker compose to start the Node Red and MQTT broker containers:
```
docker-compose up --force-recreate --build
```
8. Go to http://localhost:1880/ and log into Node Red with the credentials you set earlier.




## Production
### Server Set Up
You can us NGINX to serve the Node Red app. Here is an example server block for NGINX:
```
server {
        listen 80;
        listen [::]:80;

        server_name vizblocks-server.com;

        location / {
            proxy_pass http://localhost:1880;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
        }
  }
```

A certificate can be obtained for securing the server, using certbot:
```
  - apt install certbot python3-certbot-nginx -y
  - certbot --non-interactive --agree-tos --redirect -m "your-email@email.com" --nginx -d vizblocks-server.com
```
You will need to add permissions for mqtt broker container to access certificates:
```
setfacl -R -m u:1883:rX /etc/letsencrypt/archive/vizblocks-server.com
```

You will also need to ensure that if you are using a firewall, ports 1883 and 8883 are open for MQTT connections.

### Docker set up

1. [Install Docker and Docker Compose](https://docs.docker.com/compose/install/).
2. [Install Mosquitto](https://mosquitto.org/download/).
3. Duplicate the `production_config_example` folder and rename it to `production_config`.
4. On lines 126 and 127 in [production_config/node-red-data/settings.js](production_config/node-red-data/settings.js), replace `<NODE_RED_PROD_USER>` and `<NODE_RED_PROD_PASSWORD>`  with the username and password that you'd like to use. *Note: passwords should be encrypted using bcrypt, see [here](https://nodered.org/docs/user-guide/runtime/securing-node-red)*.
5. In [production_config/mosquitto/credentials.txt](production_config/mosquitto/credentials.txt), replace `<MQTT_PROD_USER>` and `<MQTT_PROD_PASSWORD>`  with the plain text username and password that you want to use for MQTT authentication (e.g. `user:password`).
5. In a terminal run the following command to encrypt your MQTT password file:
```
mosquitto_passwd -U production_config/mosquitto/credentials.txt
```
6. Replace `<SERVER_URL>` in [production/mosquitto/config/mosquitto.conf](production/mosquitto/config/mosquitto.conf) with the url of your server (e.g. vizblocks-server.com).
7. Create a `.env` file and add an environment variable named `SERVER_URL` with a value set to your server url (e.g. `SERVER_URL=vizblocks-server.com`)
2. In a terminal, navigate to the project root and use docker compose to start the Node Red and MQTT broker containers:
```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --build --force-recreate
```

## Testing with a VizBlock
When you open Node Red you should get an option to open a new project. Select 'Not Right Now' to load the default flow.

This flow lets you test the basic functionality of this remote Node-Red and MQTT server.

Before starting, check that the purple MQTT send node says `connected` beneath it. If not, double click the node to open its settings, then click the pencil icon next to the server input, then go to the security tab and make sure the username and password is entered for the MQTT broker.

### Setting up your VizBlock
In order for a the vizblock to work with this remote server, you will need configure the VizBlock (using Arduino code) so that it:
 a) connects to a WiFi network with internet access.
 b) connects to the remote MQTT server.
 
You can do this by editing the block configuration in the following lines:

```
VizBlock node(
  "null",       // Our ID
  "<wifi_net>",  // Wifi Access Point
  "<wifi_password>",  // WiFi Password
  "<mqtt_address>",// IP address or URL of MQTT broker
   <mqtt_port>,         // Port for MQTT broker
   "<mqtt_user>",  // MQTT username
   "<mqtt_password>",    // MQTT password (unencrypted)
   "<fingerprint>" // SHA1 fingerprint (for mqtt server)
  );
```
Replace all of the settings in `<>` with your values (or those provided to you). To get the SHA1 fingerprint you can use the following command (on Mac/Linux):

```bash
openssl s_client -connect vizblocks-server.com:8883 < /dev/null 2>/dev/null | openssl x509 -fingerprint -noout -in /dev/stdin
```

### Running a test
Double click on the blue input node to open its setting tab, then enter the command you'd like to send to the VizBlock in the `msg.payload` field.

Double click on the purple MQTT node and enter the name of the VizBlock you'd like to control in the Topic field.

Click **Deploy** in the top right corner, then click the button to the right of the input node to send commands to your VizBlock.