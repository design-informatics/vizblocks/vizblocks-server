terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "1.46.0"
    }
  }
}

provider "openstack" {
   cloud = "openstack" 
}


variable "vizblocks_server" {
   type = map
}

resource "openstack_compute_instance_v2" "vizblocks_server" {
  name            = "vizblocks-server"
  image_id        = "0818fc30-9104-4a16-9594-54af2b4ee939"
  flavor_id       = "3"
  key_pair        = "mykey"
  security_groups = ["default", "mqtt"]
  user_data = templatefile("cloud-config-vizblocks-server.yml", var.vizblocks_server)  

  metadata = {
    this = "that"
  }

  network {
    name = "VM Network Public"
  }
}

resource "openstack_compute_floatingip_associate_v2" "vizblocks_server_ip" {
   floating_ip = "129.215.193.218"
   instance_id = openstack_compute_instance_v2.vizblocks_server.id
   fixed_ip    = openstack_compute_instance_v2.vizblocks_server.network.0.fixed_ip_v4
}